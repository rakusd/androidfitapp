package com.example.burner

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.text.bold
import com.example.burner.exercises.Exercise
import com.example.burner.helper.FacebookHelper
import com.example.burner.helper.SQLHelper
import com.facebook.share.widget.ShareDialog
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.defaultSharedPreferences
import org.jetbrains.anko.doAsync
import java.io.Serializable
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    var user: FirebaseUser? = null

    private val indoorCode = 1
    private val authCode = 2

    private val shareDialog by lazy { ShareDialog(this) }

    private val defaultExercises by lazy { mutableListOf<Exercise>(
        Exercise(
            getString(R.string.pushUpsName),
            resources.getStringArray(R.array.pushUpsDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.pushUpsDetailsString),
            getString(R.string.pushUpsDescription),
            200f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_boy_doing_pushups
        ),
        Exercise(
            getString(R.string.squatsName),
            resources.getStringArray(R.array.squatsDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.squatsDetailsString),
            getString(R.string.squatsDescription),
            100f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_man_exercising_his_legs
        ),
        Exercise(
            getString(R.string.jumpingJacksName),
            resources.getStringArray(R.array.jumpingJacksDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                3
            )].toFloat(),
            getString(R.string.jumpingJacksDetailsString),
            getString(R.string.jumpingJacksDescription),
            150f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_man_jumping_with_arms_raised
        ),
        Exercise(
            getString(R.string.runningInPlaceName),
            resources.getStringArray(R.array.runningInPlaceDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                3
            )].toFloat(),
            getString(R.string.runningInPlaceDetailsString),
            getString(R.string.runningInPlaceDescription),
            300f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_running_man
        ),
        Exercise(
            getString(R.string.marchingName),
            resources.getStringArray(R.array.marchingDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.marchingDetailsString),
            getString(R.string.marchingDescription),
            60f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_military_parade
        ),
        Exercise(
            getString(R.string.crunchesName),
            resources.getStringArray(R.array.crunchesDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.crunchesDetailsString),
            getString(R.string.crunchesDescription),
            150f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_abs
        ),
        Exercise(
            getString(R.string.bicycleCrunchesName),
            resources.getStringArray(R.array.bicycleCrunchesDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.bicycleCrunchesDetailsString),
            getString(R.string.bicycleCrunchesDescription),
            200f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_fitness
        ),
        Exercise(
            getString(R.string.bicepCurlsName),
            resources.getStringArray(R.array.bicepCurlsDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.bicepCurlsDetailsString),
            getString(R.string.bicepCurlsDescription),
            150f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_man_lifting_weight
        ),
        Exercise(
            getString(R.string.thePlankName),
            resources.getStringArray(R.array.thePlankDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.thePlankDetailsString),
            getString(R.string.thePlankDescription),
            100f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_plank
        ),
        Exercise(
            getString(R.string.benchDipsName),
            resources.getStringArray(R.array.benchDipsDetailsFloats)[defaultSharedPreferences.getInt(
                "level",
                2
            )].toFloat(),
            getString(R.string.benchDipsDetailsString),
            getString(R.string.benchDipsDescription),
            160f,
            resources.getInteger(R.integer.defaultRating),
            R.drawable.ic_bench
        )
    )}

    lateinit var exercises:MutableList<Exercise>

    private val list = listOf(
        "donuts" to 280,
        "carrots" to 25,
        "cherries" to 4,
        "bananas" to 111,
        "big macs" to 540,
        "boiled eggs" to 78,
        "french fries" to 3,
        "cans of coke" to 139,
        "chocolate chip cookies" to 49,
        "heads of iceberg lettuce" to 109
    )

    private val list_ic = listOf(
        R.drawable.ic_donut,
        R.drawable.ic_carrot,
        R.drawable.ic_cherry,
        R.drawable.ic_banana,
        R.drawable.ic_burger,
        R.drawable.ic_boiled,
        R.drawable.ic_french_fries,
        R.drawable.ic_cola,
        R.drawable.ic_cookies,
        R.drawable.ic_lettuce
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Choose authentication providers
        val providers = arrayListOf(
            AuthUI.IdpConfig.EmailBuilder().build())

        user = FirebaseAuth.getInstance().currentUser

        // Create and launch sign-in intent only if user is not signed in
        if(user == null ) {
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(), authCode)
        }

        settingsButton.setOnClickListener {
            val settingsIntent= Intent(this,SettingsActivity::class.java)
            startActivity(settingsIntent)
        }

        indoorButton.setOnClickListener {
            var chosen:MutableList<Exercise> = chooseExercises(5)
            val indoorIntent=Intent(this,IndoorActivity::class.java)
            val serializableChosen:Serializable=chosen as Serializable
            indoorIntent.putExtra("Exercises",serializableChosen)
            startActivityForResult(indoorIntent,indoorCode)
        }

        statsButton.setOnClickListener {
            val statsIntent=Intent(this,StatisticsActivity::class.java)
            startActivity(statsIntent)
        }
        outdoorButton.setOnClickListener {
            val outdoorIntent=Intent(this,OutdoorActivity::class.java)
            startActivity(outdoorIntent)
        }

        updateSummary()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Tutaj przyjdą ostatnio spalone kalorie jako data "burnedCalories"
        when(requestCode) {
            indoorCode -> {
                if(resultCode== Activity.RESULT_OK) {
                    val burned = data?.getFloatExtra("burnedCalories",0f)?:0f
                    val who:String = defaultSharedPreferences.getString(getString(R.string.displayName),"John Smith")?:"John Smith"
                    if(burned==0f) {
                        Snackbar.make(mainRootLayout,String.format("Maybe next time, %s ;)",who),Snackbar.LENGTH_LONG)
                            .show()
                        return
                    }
                    Snackbar.make(mainRootLayout,String.format("Congratulations %s, you've just burnt %.02f calories",who,burned),Snackbar.LENGTH_LONG)
                        .setAction("Share") { FacebookHelper.shareBurnedCalories(shareDialog,burned) }
                        .show()
                    var total = defaultSharedPreferences.getFloat("totalCalories",0f)
                    total+=burned
                    if(user!=null) {
                        doAsync {
                            SQLHelper.UpdateOrInsertCalories(user?.email!!,total)
                        }
                    }
                    val editor = defaultSharedPreferences.edit()
                    editor.putFloat("totalCalories",total)
                    editor.putFloat("lastIndoorCalories",burned)
                    editor.commit()
                }
            }
            authCode -> {
                val response = IdpResponse.fromResultIntent(data)

                if (resultCode == Activity.RESULT_OK) {
                    // Successfully signed in
                    user = FirebaseAuth.getInstance().currentUser
                    // ...
                } else {
                    // Sign in failed. If response is null the user canceled the
                    // sign-in flow using the back button. Otherwise check
                    // response.getError().getErrorCode() and handle the error.
                    // ...
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        updateListofExercises()
        updateSummary()
    }

    private fun updateListofExercises() {
        defaultExercises.clear()
        val prefs = getSharedPreferences("com.example.burner_preferences", MODE_PRIVATE)
        val level = prefs.getInt("level",2)
        val kg = prefs.getString("Weight", "80.0")!!.toFloat()
        val tempList:MutableList<Exercise> = mutableListOf<Exercise>(
            Exercise(
                getString(R.string.pushUpsName),
                resources.getStringArray(R.array.pushUpsDetailsFloats)[level].toFloat(),
                getString(R.string.pushUpsDetailsString),
                getString(R.string.pushUpsDescription),
                2f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.pushUpsRating), 3),
                R.drawable.ic_boy_doing_pushups
            ),
            Exercise(
                getString(R.string.squatsName),
                resources.getStringArray(R.array.squatsDetailsFloats)[level].toFloat(),
                getString(R.string.squatsDetailsString),
                getString(R.string.squatsDescription),
                1f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.squatsRating), 3),
                R.drawable.ic_man_exercising_his_legs
            ),
            Exercise(
                getString(R.string.jumpingJacksName),
                resources.getStringArray(R.array.jumpingJacksDetailsFloats)[level].toFloat(),
                getString(R.string.jumpingJacksDetailsString),
                getString(R.string.jumpingJacksDescription),
                1.5f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.jumpingJacksRating), 3),
                R.drawable.ic_man_jumping_with_arms_raised
            ),
            Exercise(
                getString(R.string.runningInPlaceName),
                resources.getStringArray(R.array.runningInPlaceDetailsFloats)[level].toFloat(),
                getString(R.string.runningInPlaceDetailsString),
                getString(R.string.runningInPlaceDescription),
                3f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.runningInPlaceRating), 3),
                R.drawable.ic_running_man
            ),
            Exercise(
                getString(R.string.marchingName),
                resources.getStringArray(R.array.marchingDetailsFloats)[level].toFloat(),
                getString(R.string.marchingDetailsString),
                getString(R.string.marchingDescription),
                0.5f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.marchingRating), 3),
                R.drawable.ic_military_parade
            ),
            Exercise(
                getString(R.string.crunchesName),
                resources.getStringArray(R.array.crunchesDetailsFloats)[level].toFloat(),
                getString(R.string.crunchesDetailsString),
                getString(R.string.crunchesDescription),
                1.5f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.crunchesRating), 3),
                R.drawable.ic_abs
            ),
            Exercise(
                getString(R.string.bicycleCrunchesName),
                resources.getStringArray(R.array.bicycleCrunchesDetailsFloats)[level].toFloat(),
                getString(R.string.bicycleCrunchesDetailsString),
                getString(R.string.bicycleCrunchesDescription),
                2f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.bicycleCrunchesRating), 3),
                R.drawable.ic_fitness
            ),
            Exercise(
                getString(R.string.bicepCurlsName),
                resources.getStringArray(R.array.bicepCurlsDetailsFloats)[level].toFloat(),
                getString(R.string.bicepCurlsDetailsString),
                getString(R.string.bicepCurlsDescription),
                1.5f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.bicepCurlsRating), 3),
                R.drawable.ic_man_lifting_weight
            ),
            Exercise(
                getString(R.string.thePlankName),
                resources.getStringArray(R.array.thePlankDetailsFloats)[level].toFloat(),
                getString(R.string.thePlankDetailsString),
                getString(R.string.thePlankDescription),
                1f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.thePlankRating), 3),
                R.drawable.ic_plank
            ),
            Exercise(
                getString(R.string.benchDipsName),
                resources.getStringArray(R.array.benchDipsDetailsFloats)[level].toFloat(),
                getString(R.string.benchDipsDetailsString),
                getString(R.string.benchDipsDescription),
                1.6f * (level + 1) * kg / 50f,
                defaultSharedPreferences.getInt(getString(R.string.benchDipsRating), 3),
                R.drawable.ic_bench
            )
        )
        defaultExercises.addAll(tempList)
        exercises=defaultExercises
    }

    private fun chooseExercises(count:Int):MutableList<Exercise> {
        var returnVal:MutableList<Exercise> = mutableListOf<Exercise>()
        var exerciseList:MutableList<Exercise> = exercises.map{it.copy()}.toMutableList()

        for( i in 0 until count) {
            val sumOf=exerciseList.sumBy { it.rating }
            var choice=Random.nextInt(0,sumOf+1)

            returnVal.add(selectExercise(exerciseList,choice))
        }

        return returnVal
    }

    private fun selectExercise(exerciseList:MutableList<Exercise>, choice:Int): Exercise {
        var chosenExercise: Exercise =exerciseList[0]
        var userChoice=choice

        for(i in 0 until exerciseList.size) {
            if(userChoice<exerciseList[i].rating) {
                chosenExercise=exerciseList[i]
                exerciseList.removeAt(i)
                break
            } else {
                userChoice-=exerciseList[i].rating
            }
        }

        return chosenExercise
    }

    private fun updateSummary(){
        val prefs = getSharedPreferences("com.example.burner_preferences", MODE_PRIVATE)
        val f = prefs.getFloat("totalCalories", 0f)
        summary1.text = SpannableStringBuilder()
            .append("You have burned a total of\n")
            .bold{ append(String.format("%.02f kcal.", f)) }
        val r = Random.nextInt(list.size)
        val v = list[r]
        summary2.text = SpannableStringBuilder()
            .append("That's about as much as\n")
            .bold{ append(String.format("%.02f %s.", f/v.second, v.first)) }
        summary2.setCompoundDrawablesWithIntrinsicBounds( list_ic[r], 0, 0, 0)
    }

}
