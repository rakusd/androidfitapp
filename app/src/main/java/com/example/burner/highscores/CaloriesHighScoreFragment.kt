package com.example.burner.highscores

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.burner.R

import com.example.burner.helper.SQLHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.runOnUiThread

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [CaloriesHighScoreFragment.OnListFragmentInteractionListener] interface.
 */
class CaloriesHighScoreFragment : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_highscore_list, container, false)
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyHighScoreRecyclerViewAdapter(mutableListOf(),context)
                doAsync {
                    val highScores = SQLHelper.getCaloriesLeaderboards()
                    runOnUiThread {
                        (adapter as MyHighScoreRecyclerViewAdapter).mValues=highScores
                        (adapter as MyHighScoreRecyclerViewAdapter).notifyDataSetChanged()
                    }
                }
            }
        }
        return view
    }
}
