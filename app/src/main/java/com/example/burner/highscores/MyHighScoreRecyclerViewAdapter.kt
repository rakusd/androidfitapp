package com.example.burner.highscores

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.burner.R
import kotlinx.android.synthetic.main.fragment_highscore.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class MyHighScoreRecyclerViewAdapter(
    var mValues: List<HighScoreItem>,
    val context: Context
) : RecyclerView.Adapter<MyHighScoreRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_highscore, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(position==0) {
            holder.mIdView.text = "#"
            holder.mUserView.text = "User"
            holder.mScoreView.text = "Score"
            holder.mLayoutView.background = context.getDrawable(R.drawable.gradient_tile)
        } else {
            val item = mValues[position-1]
            holder.mIdView.text = (position).toString()
            holder.mUserView.text = item.user
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            holder.mScoreView.text = df.format(item.score)
        }

    }

    override fun getItemCount(): Int = mValues.size+1

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mUserView: TextView = mView.user
        val mScoreView: TextView = mView.score
        val mLayoutView = mView.highScoreItemLayout

        override fun toString(): String {
            return super.toString() + " '" + mUserView.text + "'"+mScoreView.text
        }
    }
}
