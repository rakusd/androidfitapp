package com.example.burner.highscores

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.burner.R
import kotlinx.android.synthetic.main.fragment_statistics.*
import org.jetbrains.anko.support.v4.defaultSharedPreferences
import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * A placeholder fragment containing a simple view.
 */
class YourStatsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_statistics, container, false)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        kcalTextView.text=df.format(defaultSharedPreferences.getFloat("totalCalories",0f))
        distanceTextView.text=df.format(defaultSharedPreferences.getFloat("totalDistanceTraveled",0f))
        cyclingTextView.text=df.format(defaultSharedPreferences.getFloat("totalDistanceCycled",0f))
        runningTextView.text=df.format(defaultSharedPreferences.getFloat("totalDistanceRun",0f))
        walkingTextView.text=df.format(defaultSharedPreferences.getFloat("totalDistanceWalked",0f))
        pushupstextView.text=df.format(defaultSharedPreferences.getInt("pushUpsCount",0))
        squatsTextView.text=df.format(defaultSharedPreferences.getInt("squatsCount",0))
        jumpingJacksTextView.text=df.format(defaultSharedPreferences.getInt("jumpingJacksCount",0))
        runningInPlaceTextView.text=df.format(defaultSharedPreferences.getFloat("runningInPlaceTime",0f))
        marchingTextView.text=df.format(defaultSharedPreferences.getFloat("marchingTime",0f))
        crunchesTextView.text=df.format(defaultSharedPreferences.getInt("crunchesCount",0))
        bicycleCrunchesTextView.text=df.format(defaultSharedPreferences.getFloat("bicycleCrunchesTime",0f))
        bicepCurlsTextView.text=df.format(defaultSharedPreferences.getInt("bicepsCurlsCount",0))
        plankTextView.text=df.format(defaultSharedPreferences.getFloat("thePlankTime",0f))
        benchDipsTextView.text=df.format(defaultSharedPreferences.getInt("benchDipsCount",0))
    }
}