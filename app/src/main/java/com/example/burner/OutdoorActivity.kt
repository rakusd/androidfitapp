package com.example.burner

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_outdoor.*

class OutdoorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outdoor)

        val perms = arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, perms, 0)

        cyclingButton.setOnClickListener {
            val exerciseIntent= Intent(this, OutdoorExerciseActivity::class.java)
            exerciseIntent.putExtra("type", OutdoorExerciseType.Cycling.ordinal)
            startActivity(exerciseIntent)
        }
        walkingButton.setOnClickListener {
            val exerciseIntent= Intent(this, OutdoorExerciseActivity::class.java)
            exerciseIntent.putExtra("type", OutdoorExerciseType.Walking.ordinal)
            startActivity(exerciseIntent)
        }
        runningButton.setOnClickListener {
            val exerciseIntent= Intent(this, OutdoorExerciseActivity::class.java)
            exerciseIntent.putExtra("type", OutdoorExerciseType.Running.ordinal)
            startActivity(exerciseIntent)
        }
        rollerSkatingButton.setOnClickListener {
            val exerciseIntent= Intent(this, OutdoorExerciseActivity::class.java)
            exerciseIntent.putExtra("type", OutdoorExerciseType.MTB.ordinal)
            startActivity(exerciseIntent)
        }
    }
}

enum class OutdoorExerciseType{
    Running, Walking, Cycling, MTB
}
