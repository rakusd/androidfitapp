package com.example.burner.exercises

import java.io.Serializable

data class Exercise(val name:String,val detailsFloat:Float,val detailsString:String, val description:String, val baseCalories:Float,var rating:Int,var image:Int ):Serializable {
}