package com.example.burner.exercises

import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.exercise.view.*

class ExerciseViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val title=itemView.titleTextView
    val details = itemView.detailsTextView
    val calories=itemView.caloriesTextView
    val checkbox = itemView.checkbox
    val img = itemView.image

    fun bindItem(exercise: Exercise) {
        title.text=exercise.name
        var strVal:String
        val compareFloat:Float=exercise.detailsFloat.toInt().toFloat()
        strVal = if(compareFloat==exercise.detailsFloat) {
            compareFloat.toInt().toString()
        } else {
            exercise.detailsFloat.toString()
        }
        details.text=strVal+" "+exercise.detailsString
        calories.text=exercise.baseCalories.toString() + " kcal"
        img.setImageResource(exercise.image)
    }
}