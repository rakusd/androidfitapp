package com.example.burner.exercises

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.LiveData

class ExerciseViewModel: ViewModel() {

    private val exerciseLiveData = MutableLiveData<MutableList<Exercise>>()

    fun getItems():LiveData<MutableList<Exercise>> = exerciseLiveData
    //fun isEmpty():Boolean =exerciseLiveData.value?.isEmpty() ?: true

    fun setItems(exercises:MutableList<Exercise>) {
        exerciseLiveData.value=exercises
    }

}