package com.example.burner

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.burner.exercises.Exercise
import com.example.burner.exercises.ExerciseAdapter
import com.example.burner.exercises.ExerciseViewHolder
import com.example.burner.exercises.ExerciseViewModel
import kotlinx.android.synthetic.main.activity_indoor.*
import org.jetbrains.anko.defaultSharedPreferences

class IndoorActivity : AppCompatActivity() {

    lateinit var exercises:MutableList<Exercise>
    private val viewModel: ExerciseViewModel by lazy { ViewModelProviders.of(this).get(ExerciseViewModel::class.java)}
    private val adapter: ExerciseAdapter by lazy {
        ExerciseAdapter(
            mutableListOf()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_indoor)

        recyclerView.adapter=adapter
        viewModel.getItems().observe(this, Observer(this::updateList))

        exercises = intent.getSerializableExtra("Exercises") as MutableList<Exercise>
        viewModel.setItems(exercises)

        finishButton.setOnClickListener {
            var burned = 0f
            for (i in 0 until recyclerView.childCount) {
                val holder = recyclerView.findViewHolderForAdapterPosition(i) as ExerciseViewHolder
                if(holder.checkbox.isChecked) {
                    burned+=holder.calories.text.toString().substringBefore(' ').toFloat()
                    when(holder.title.text) {
                        getString(R.string.pushUpsName) -> {increaseStats("pushUpsCount",holder.details.text.toString().substringBefore(' ').toInt())}
                        getString(R.string.squatsName) -> {increaseStats("squatsCount",holder.details.text.toString().substringBefore(' ').toInt())}
                        getString(R.string.jumpingJacksName) -> {increaseStats("jumpingJacksCount",holder.details.text.toString().substringBefore(' ').toInt())}
                        getString(R.string.runningInPlaceName) -> {increaseStats("runningInPlaceTime",holder.details.text.toString().substringBefore(' ').toFloat())}
                        getString(R.string.marchingName) -> {increaseStats("marchingTime",holder.details.text.toString().substringBefore(' ').toFloat())}
                        getString(R.string.crunchesName) -> {increaseStats("crunchesCount",holder.details.text.toString().substringBefore(' ').toInt())}
                        getString(R.string.bicycleCrunchesName) -> {increaseStats("bicycleCrunchesTime",holder.details.text.toString().substringBefore(' ').toFloat())}
                        getString(R.string.bicepCurlsName) -> {increaseStats("bicepsCurlsCount",holder.details.text.toString().substringBefore(' ').toInt())}
                        getString(R.string.thePlankName) -> {increaseStats("thePlankTime",holder.details.text.toString().substringBefore(' ').toFloat())}
                        getString(R.string.benchDipsName) -> {increaseStats("benchDipsCount",holder.details.text.toString().substringBefore(' ').toInt())}
                    }
                }
            }
            intent.putExtra("burnedCalories",burned)
            setResult(Activity.RESULT_OK,intent)
            finish()
        }

        cancelButton.setOnClickListener {
            intent.putExtra("burnedCalories",0f)
            finish()
        }
    }

    private fun increaseStats(statsName:String,value:Float) {
        var stats = defaultSharedPreferences.getFloat(statsName,0f)
        stats+=value
        defaultSharedPreferences.edit()
            .putFloat(statsName,value)
            .apply()
    }

    private fun increaseStats(statsName: String,value:Int) {
        var stats = defaultSharedPreferences.getInt(statsName,0)
        stats+=value
        defaultSharedPreferences.edit()
            .putInt(statsName,stats)
            .apply()

    }

    private fun updateList(exercises:MutableList<Exercise>?) {
        if(exercises==null)
            return
        adapter.items=exercises
        adapter.notifyDataSetChanged()
    }
}
