package com.example.burner.helper

import com.example.burner.highscores.HighScoreItem
import java.sql.Connection
import java.sql.DriverManager
import kotlin.math.round

object SQLHelper {
    private val connectionString = "jdbc:jtds:sqlserver://jobofferdamianrakus.database.windows.net:1433/MyDatabase;user=rakusd@jobofferdamianrakus;password=1234Test;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;"
    private val classString = "net.sourceforge.jtds.jdbc.Driver"

    fun getCaloriesLeaderboards():MutableList<HighScoreItem> {
        Class.forName(classString)
        val conn: Connection = DriverManager.getConnection(connectionString)
        val statement = conn.createStatement()
        val queryString = "select top 10 * from CaloriesHighScores order by Calories desc"
        val rs = statement.executeQuery(queryString)
        val highScores:MutableList<HighScoreItem> = mutableListOf()

        while(rs.next()) {
            val user = rs.getString("Username")
            val score = rs.getFloat("Calories")
            highScores.add(HighScoreItem(user,score))
        }
        conn.close()

        return highScores
    }

    fun getDistanceLeaderboards():MutableList<HighScoreItem> {
        Class.forName(classString)
        val conn: Connection = DriverManager.getConnection(connectionString)
        val statement = conn.createStatement()
        val queryString = "select top 10 * from DistanceHighScores order by Distance desc"
        val rs = statement.executeQuery(queryString)
        val highScores:MutableList<HighScoreItem> = mutableListOf()

        while(rs.next()) {
            val user = rs.getString("Username")
            val score = rs.getFloat("Distance")
            highScores.add(HighScoreItem(user,score))
        }
        conn.close()

        return highScores
    }

    fun UpdateOrInsertCalories(user:String,calories:Float) {
        Class.forName(classString)
        val conn: Connection = DriverManager.getConnection(connectionString)
        val statementString = "begin tran " +
                "if exists (select * from CaloriesHighScores where Username = ? )" +
                "begin " +
                "update CaloriesHighScores set Calories = ? where Username = ? " +
                "end " +
                "else " +
                "begin " +
                "insert into CaloriesHighScores values (?,?) " +
                "end " +
                "commit tran "
        val preparedStatement = conn.prepareStatement(statementString)
        preparedStatement.setString(1,user)
        preparedStatement.setFloat(2,calories)
        preparedStatement.setString(3,user)
        preparedStatement.setString(4,user)
        preparedStatement.setFloat(5,calories)

        preparedStatement.execute()
        conn.close()
    }

    fun UpdateOrInsertDistance(user:String,distance:Float) {
        Class.forName(classString)
        val conn: Connection = DriverManager.getConnection(connectionString)
        val statementString = "begin tran " +
                "if exists (select * from DistanceHighScores where Username = ? )" +
                "begin " +
                "update DistanceHighScores set Distance = ? where Username = ? " +
                "end " +
                "else " +
                "begin " +
                "insert into DistanceHighScores values (?,?) " +
                "end " +
                "commit tran "
        val preparedStatement = conn.prepareStatement(statementString)
        preparedStatement.setString(1,user)
        preparedStatement.setFloat(2,distance)
        preparedStatement.setString(3,user)
        preparedStatement.setString(4,user)
        preparedStatement.setFloat(5,distance)
        preparedStatement.execute()
        conn.close()
    }
}