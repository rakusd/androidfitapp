package com.example.burner.helper

import android.graphics.Bitmap
import android.net.Uri
import com.example.burner.OutdoorExerciseType
import com.facebook.share.model.*
import com.facebook.share.widget.ShareDialog

object FacebookHelper {
    fun shareRun(shareDialog: ShareDialog,calories: Float,distance:Float,type: OutdoorExerciseType) {
        if(ShareDialog.canShow(ShareLinkContent::class.java)) {
            var exerciseType:String=""
            when(type) {
                OutdoorExerciseType.Cycling -> exerciseType="cycling"
                OutdoorExerciseType.Running -> exerciseType="running"
                OutdoorExerciseType.Walking -> exerciseType="walking"
                OutdoorExerciseType.MTB -> exerciseType="riding mountain bike"
            }
            val content = ShareLinkContent.Builder()
                .setQuote("I have just burnt "+calories.toString()+" kcal while "+exerciseType+" and I have covered a distance of "+distance.toString()+"km using Burner App. Download it for free and join me in the struggle to get fit")
                .setContentUrl(Uri.parse("https://example.org/"))
                .setShareHashtag(ShareHashtag.Builder().setHashtag("#Burner").build())
                .build()

            shareDialog.show(content,ShareDialog.Mode.WEB)
        }
    }
    fun shareBurnedCalories(shareDialog:ShareDialog,calories:Float) {
        if(ShareDialog.canShow(ShareLinkContent::class.java)) {

            val content = ShareLinkContent.Builder()
                .setQuote("I have just burnt "+calories.toString()+" kcal using Burner App. Download it for free and join me in the struggle to get fit")
                .setContentUrl(Uri.parse("https://example.org/"))
                .setShareHashtag(ShareHashtag.Builder().setHashtag("#Burner").build())
                .build()

            shareDialog.show(content,ShareDialog.Mode.WEB)

        }
    }

    fun shareResults(shareDialog: ShareDialog,bitmap: Bitmap) {

        val photo = SharePhoto.Builder()
            .setBitmap(bitmap)
            .setCaption("Hard work and dedication tracked by Burner")
            .build()

        val content = SharePhotoContent.Builder()
            .setShareHashtag(ShareHashtag.Builder().setHashtag("#Burner").build())
            .addPhoto(photo)
            .build()

        shareDialog.show(content,ShareDialog.Mode.WEB)
    }
}