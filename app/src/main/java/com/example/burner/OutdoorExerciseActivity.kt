package com.example.burner

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_outdoor_exercise.*
import android.location.LocationManager
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationListener
import android.location.Location
import android.util.TypedValue
import android.view.View
import androidx.core.app.ActivityCompat
import com.example.burner.helper.FacebookHelper
import com.example.burner.helper.SQLHelper
import com.facebook.share.widget.ShareDialog
import com.firebase.ui.auth.data.model.User
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.doAsync


class OutdoorExerciseActivity : AppCompatActivity(), OnMapReadyCallback {

    var type = OutdoorExerciseType.Walking
    var start = System.currentTimeMillis()
    val shareDialog:ShareDialog by lazy {ShareDialog(this)}
    val locationListener = MyLocationListener()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outdoor_exercise)
        val b = intent.extras
        type = OutdoorExerciseType.values()[b!!.getInt("type")]
        when(type){
            OutdoorExerciseType.Cycling -> currentOutdoorExerciseTextView.text = getString(R.string.cycling)
            OutdoorExerciseType.Running -> currentOutdoorExerciseTextView.text = getString(R.string.running)
            OutdoorExerciseType.Walking -> currentOutdoorExerciseTextView.text = getString(R.string.walking)
            OutdoorExerciseType.MTB -> currentOutdoorExerciseTextView.text = getString(R.string.MTB)
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(outdoorExerciseLayout, "First enable LOCATION ACCESS in settings.", Snackbar.LENGTH_LONG).show()
            super.onBackPressed()
            return
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 5f, locationListener)
        (map as SupportMapFragment).getMapAsync(this)

        finishButton.setOnClickListener {
            val km = locationListener.dist/1000
            val min = (System.currentTimeMillis()-start)/6e4
            val kcal = getKcal(km/min*50/3, min)
            val prefs = getSharedPreferences("com.example.burner_preferences", MODE_PRIVATE)
            val editor = prefs.edit()

            val totalCalories =  prefs.getFloat("totalCalories", 0f) + kcal.toFloat()
            val totalDistance = prefs.getFloat("totalDistanceTraveled", 0f) + km.toFloat()

            val user = FirebaseAuth.getInstance().currentUser
            if(user!=null) {
                doAsync {
                    SQLHelper.UpdateOrInsertCalories(user.email!!,totalCalories)
                    SQLHelper.UpdateOrInsertDistance(user.email!!,totalDistance)
                }
            }

            editor.putFloat("totalCalories",totalCalories)
            editor.putFloat("totalDistanceTraveled",totalDistance)

            when(type)
            {
                OutdoorExerciseType.Cycling -> editor.putFloat("totalDistanceCycled", prefs.getFloat("totalDistanceCycled", 0f) + km.toFloat())
                OutdoorExerciseType.Running -> editor.putFloat("totalDistanceRun", prefs.getFloat("totalDistanceRun", 0f) + km.toFloat())
                OutdoorExerciseType.Walking -> editor.putFloat("totalDistanceWalked", prefs.getFloat("totalDistanceWalked", 0f) + km.toFloat())
                OutdoorExerciseType.MTB -> editor.putFloat("totalDistanceCycled", prefs.getFloat("totalDistanceCycled", 0f) + km.toFloat())
            }
            editor.commit()
            currentOutdoorExerciseTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            currentOutdoorExerciseTextView.text = String.format("You have traveled %.02f km in %.01f min\nburning a total of %.01f kcal\nYour average speed was %.02f km/h",
                km, min, kcal, 60*km/min)
            finishButton.visibility = View.GONE
            Snackbar.make(outdoorExerciseLayout,String.format("Share the results with friends",
                km, min, kcal, 60*km/min),Snackbar.LENGTH_INDEFINITE)
                .setAction("Share") { FacebookHelper.shareRun(shareDialog,kcal.toFloat(),km.toFloat(),type)}
                .show()
        }
    }

    override fun onMapReady(ready: GoogleMap?) {
        locationListener.setMap(ready)
    }

    private fun getKcal(mps : Double, min : Double) : Double {
        val prefs = getSharedPreferences("com.example.burner_preferences", MODE_PRIVATE)
        val kg = prefs.getString("Weight", "80.0")!!.toFloat()
        val h = prefs.getString("Height", "180.0")!!.toFloat()
        var MET = 2.2
        return when(type) {
            OutdoorExerciseType.Walking -> min * kg * (0.035 + mps * mps / h * 0.029)
            OutdoorExerciseType.Cycling -> {
                MET += mps * (0.129 * mps + 0.251)
                min * MET * 3.5 * kg / 200
            }
            OutdoorExerciseType.MTB -> {
                MET += mps * (0.16 * mps + 0.339)
                min * MET * 3.5 * kg / 200
            }
            OutdoorExerciseType.Running -> {
                MET += mps * (0.239 * mps + 1.242)
                min * MET * 3.5 * kg / 200
            }
        }
    }


    class MyLocationListener() : LocationListener {
        var loctext = "no update"
        var dist = 0.0
        var prev:Location? = null
        private var gmap:GoogleMap? = null

        override fun onLocationChanged(loc: Location?) {

            if(loc != null) {
                if (prev != null) {
                    dist += loc.distanceTo(prev)
                    if(gmap != null)
                    {
                        val opt = PolylineOptions().width(5f).color(Color.RED).geodesic(true)
                        opt.add(LatLng(prev!!.latitude, prev!!.longitude))
                        opt.add(LatLng(loc.latitude, loc.longitude))
                        gmap!!.addPolyline(opt)
                    }
                }
                prev = loc
                loctext = "Lat: " + loc.latitude + " Lng: " + loc.longitude
                if(gmap != null) {
                    val latLng = LatLng(loc.latitude, loc.longitude)
                    val zoomLevel = 16.0f
                    gmap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
                }
            }
        }

        fun setMap(m: GoogleMap?)
        {
            gmap = m
        }


        override fun onProviderDisabled(provider:String) {}

        override fun onProviderEnabled(provider:String) {}

        override fun onStatusChanged(provider:String, status:Int, extras:Bundle) {}
    }
}
